## README

This service exposes a few APIs related to accounts and contacts. The entities are stored in an in-memory H2 database, and are **not** persisted between application sessions. The service traffic does not yet support HTTPS.

#### Requirements:
* Maven installed and on your PATH
* Java 8 (or later) installed

#### Setup
1. Clone the repository
```
git clone
```
2. Build the application
```
mvn install
```
3. Start the service
```
mvn spring-boot:run
```

#### Model

##### Account

```
{
    "id": long,
    "companyName": string,
    "address1": string,
    "address2": string,
    "city": string,
    "state": string,
    "zip": string,
    "country": string,
    "contacts": Contact[]
}
```

##### Contact

```
{
    "id": long,
    "name": String,
    "email": string,
    "address1": string,
    "address2": string,
    "city": string,
    "state": string,
    "zip": string,
    "country": string,
    "account": Account
}
```

#### API

##### Get Account

```
GET http://localhost/accounts/{id}
```

##### Create Account

```
POST http://localhost/accounts
```

```
{
    "companyName": "Globe Corp",
    "address1": "321 Main St.",
    "address2": "Apt B",
    "city": "Seattle",
    "state": "WA",
    "zip": "98101",
    "country": "United States",
    "contacts" : [
        {
            "name": "Jim Bob",
            "email": "jim@mail.com",
            "address1": "1 West Ave.",
            "address2": "Unit 2",
            "city": "Fairbanks",
            "state": "AK",
            "zip": "56001",
            "country": "United States"
        }
    ]
}
```

##### Update Account

```
PUT http://localhost/accounts/{id}
```
```
{
    "id": 24,
    "companyName": "Globe Corp",
    "address1": "321 Main St.",
    "address2": "Apt B",
    "city": "Seattle",
    "state": "WA",
    "zip": "98101",
    "country": "United States",
}
```

##### Get Contact

```
GET http://localhost/contacts/{id}
```

##### Create Contact

```
POST http://localhost/contacts
```

```
{
    "name": "Jim Bob",
    "email": "jim@mail.com",
    "address1": "1 West Ave.",
    "address2": "Unit 2",
    "city": "Fairbanks",
    "state": "AK",
    "zip": "56001",
    "country": "United States"
}
```

##### Update Contact

```
PUT http://localhost/contacts/{id}
```
```
{
    "id": 24,
    "name": "Jim Bob",
    "email": "jim@mail.com",
    "address1": "1 West Ave.",
    "address2": "Unit 2",
    "city": "Fairbanks",
    "state": "AK",
    "zip": "56001",
    "country": "United States"
}
```
