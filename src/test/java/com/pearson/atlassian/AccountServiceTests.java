package com.pearson.atlassian;

import com.pearson.atlassian.model.Account;
import com.pearson.atlassian.model.Contact;
import com.pearson.atlassian.persistence.AccountRepository;
import com.pearson.atlassian.service.AccountService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTests {
	private static final long TEST_ACCOUNT_ID = 12;
	private static final String TEST_COMPANY_NAME = "test_company_name";
	private static final String TEST_ADDRESS_1 = "test_add_1";
	private static final String TEST_ADDRESS_2 = "test_add_2";
	private static final String TEST_CITY = "test_city";
	private static final String TEST_STATE = "test_state";
	private static final String TEST_ZIP = "test_zip";
	private static final String TEST_COUNTRY = "test_country";

	private static Set<Contact> TEST_CONTACTS;

	@InjectMocks
	private AccountService accountService;

	@Mock
	private AccountRepository accountRepository;

	@BeforeAll
	static void setup() {
		Contact contact1 = Contact.builder().id(12).name("contact1").build();
		Contact contact2 = Contact.builder().id(12).name("contact2").build();

		TEST_CONTACTS = new HashSet<>();
		TEST_CONTACTS.add(contact1);
		TEST_CONTACTS.add(contact2);
	}

	@Test
	public void getAccountById() {
		Account testAccount = Account.builder()
				.companyName(TEST_COMPANY_NAME)
				.address1(TEST_ADDRESS_1)
				.address2(TEST_ADDRESS_2)
				.city(TEST_CITY)
				.state(TEST_STATE)
				.zip(TEST_ZIP)
				.country(TEST_COUNTRY)
				.build();

		when(accountRepository.findById(TEST_ACCOUNT_ID)).thenReturn(Optional.of(testAccount));

		ResponseEntity<Account> account = accountService.getAccountById(TEST_ACCOUNT_ID);

		assertEquals(testAccount.getCompanyName(), account.getBody().getCompanyName());
		assertEquals(testAccount.getAddress1(), account.getBody().getAddress1());
		assertEquals(testAccount.getAddress2(), account.getBody().getAddress2());
		assertEquals(testAccount.getCity(), account.getBody().getCity());
		assertEquals(testAccount.getState(), account.getBody().getState());
		assertEquals(testAccount.getZip(), account.getBody().getZip());
		assertEquals(testAccount.getCountry(), account.getBody().getCountry());
	}

	@Test
	public void getContactsForAccount() {
		Account testAccount = Account.builder()
				.contacts(TEST_CONTACTS)
				.build();

		when(accountRepository.findById(TEST_ACCOUNT_ID)).thenReturn(Optional.of(testAccount));

		ResponseEntity<Account> account = accountService.getAccountById(TEST_ACCOUNT_ID);

		assertEquals(testAccount.getContacts(), account.getBody().getContacts());
	}

	@Test
	public void createContact() {
		Account testAccount = Account.builder()
				.companyName(TEST_COMPANY_NAME)
				.address1(TEST_ADDRESS_1)
				.address2(TEST_ADDRESS_2)
				.city(TEST_CITY)
				.state(TEST_STATE)
				.zip(TEST_ZIP)
				.country(TEST_COUNTRY)
				.contacts(TEST_CONTACTS)
				.build();

		when(accountRepository.save(testAccount)).thenReturn(testAccount);

		ResponseEntity<Account> account = accountService.add(testAccount);

		assertEquals(testAccount.getCompanyName(), account.getBody().getCompanyName());
		assertEquals(testAccount.getAddress1(), account.getBody().getAddress1());
		assertEquals(testAccount.getAddress2(), account.getBody().getAddress2());
		assertEquals(testAccount.getCity(), account.getBody().getCity());
		assertEquals(testAccount.getState(), account.getBody().getState());
		assertEquals(testAccount.getZip(), account.getBody().getZip());
		assertEquals(testAccount.getCountry(), account.getBody().getCountry());
		assertEquals(testAccount.getContacts(), account.getBody().getContacts());
	}

	@Test
	public void updateContact() {
		Account originalAccount = Account.builder()
				.companyName(TEST_COMPANY_NAME)
				.address1(TEST_ADDRESS_1)
				.address2(TEST_ADDRESS_2)
				.city(TEST_CITY)
				.state(TEST_STATE)
				.zip(TEST_ZIP)
				.country(TEST_COUNTRY)
				.contacts(TEST_CONTACTS)
				.build();

		Account updatedAccount = Account.builder()
				.companyName("new_company_name")
				.address1(TEST_ADDRESS_1)
				.address2(TEST_ADDRESS_2)
				.city("new_city")
				.state("new_state")
				.zip(TEST_ZIP)
				.country(TEST_COUNTRY)
				.contacts(TEST_CONTACTS)
				.build();

		when(accountRepository.findById(TEST_ACCOUNT_ID)).thenReturn(Optional.of(originalAccount));
		lenient().when(accountRepository.save(updatedAccount)).thenReturn(updatedAccount);

		Account account = accountService.update(TEST_ACCOUNT_ID, updatedAccount);

		assertEquals(updatedAccount.getCompanyName(), account.getCompanyName());
		assertEquals(updatedAccount.getAddress1(), account.getAddress1());
		assertEquals(updatedAccount.getAddress2(), account.getAddress2());
		assertEquals(updatedAccount.getCity(), account.getCity());
		assertEquals(updatedAccount.getState(), account.getState());
		assertEquals(updatedAccount.getZip(), account.getZip());
		assertEquals(updatedAccount.getCountry(), account.getCountry());
		assertEquals(updatedAccount.getContacts(), account.getContacts());
	}
}
