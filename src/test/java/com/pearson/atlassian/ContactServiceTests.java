package com.pearson.atlassian;

import com.pearson.atlassian.model.Account;
import com.pearson.atlassian.model.Contact;
import com.pearson.atlassian.persistence.ContactRepository;
import com.pearson.atlassian.service.ContactService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ContactServiceTests {
	private static final long TEST_CONTACT_ID = 12;
	private static final String TEST_NAME = "test_name";
	private static final String TEST_EMAIL = "test_email";
	private static final String TEST_ADDRESS_1 = "test_add_1";
	private static final String TEST_ADDRESS_2 = "test_add_2";
	private static final String TEST_CITY = "test_city";
	private static final String TEST_STATE = "test_state";
	private static final String TEST_ZIP = "test_zip";
	private static final String TEST_COUNTRY = "test_country";

	private static Account TEST_ACCOUNT;

	@InjectMocks
	private ContactService contactService;

	@Mock
	private ContactRepository contactRepository;

	@BeforeAll
	static void setup() {
		TEST_ACCOUNT = new Account();
	}

	@Test
	public void getContactById() {
		Contact testContact = Contact.builder()
				.name(TEST_NAME)
				.email(TEST_EMAIL)
				.address1(TEST_ADDRESS_1)
				.address2(TEST_ADDRESS_2)
				.city(TEST_CITY)
				.state(TEST_STATE)
				.zip(TEST_ZIP)
				.country(TEST_COUNTRY)
				.build();

		when(contactRepository.findById(TEST_CONTACT_ID)).thenReturn(Optional.of(testContact));

		ResponseEntity<Contact> contact = contactService.getContactById(TEST_CONTACT_ID);

		assertEquals(testContact.getName(), contact.getBody().getName());
		assertEquals(testContact.getEmail(), contact.getBody().getEmail());
		assertEquals(testContact.getAddress1(), contact.getBody().getAddress1());
		assertEquals(testContact.getAddress2(), contact.getBody().getAddress2());
		assertEquals(testContact.getCity(), contact.getBody().getCity());
		assertEquals(testContact.getState(), contact.getBody().getState());
		assertEquals(testContact.getZip(), contact.getBody().getZip());
		assertEquals(testContact.getCountry(), contact.getBody().getCountry());
		assertEquals(testContact.getAccount(), contact.getBody().getAccount());
	}

	@Test
	public void createContact() {
		Contact testContact = Contact.builder()
				.name(TEST_NAME)
				.email(TEST_EMAIL)
				.address1(TEST_ADDRESS_1)
				.address2(TEST_ADDRESS_2)
				.city(TEST_CITY)
				.state(TEST_STATE)
				.zip(TEST_ZIP)
				.country(TEST_COUNTRY)
				.account(TEST_ACCOUNT)
				.build();

		when(contactRepository.save(testContact)).thenReturn(testContact);

		ResponseEntity<Contact> contact = contactService.add(testContact);

		assertEquals(testContact.getName(), contact.getBody().getName());
		assertEquals(testContact.getEmail(), contact.getBody().getEmail());
		assertEquals(testContact.getAddress1(), contact.getBody().getAddress1());
		assertEquals(testContact.getAddress2(), contact.getBody().getAddress2());
		assertEquals(testContact.getCity(), contact.getBody().getCity());
		assertEquals(testContact.getState(), contact.getBody().getState());
		assertEquals(testContact.getZip(), contact.getBody().getZip());
		assertEquals(testContact.getCountry(), contact.getBody().getCountry());
		assertEquals(testContact.getAccount(), contact.getBody().getAccount());
	}

	@Test
	public void updateContact() {
		Contact originalContact = Contact.builder()
				.name(TEST_NAME)
				.address1(TEST_ADDRESS_1)
				.address2(TEST_ADDRESS_2)
				.city(TEST_CITY)
				.state(TEST_STATE)
				.zip(TEST_ZIP)
				.country(TEST_COUNTRY)
				.account(TEST_ACCOUNT)
				.build();

		Contact updatedContact = Contact.builder()
				.name("new_name")
				.address1(TEST_ADDRESS_1)
				.address2(TEST_ADDRESS_2)
				.city("new_city")
				.state("new_state")
				.zip(TEST_ZIP)
				.country(TEST_COUNTRY)
				.account(TEST_ACCOUNT)
				.build();

		when(contactRepository.findById(TEST_CONTACT_ID)).thenReturn(Optional.of(originalContact));
		lenient().when(contactRepository.save(updatedContact)).thenReturn(updatedContact);

		Contact contact = contactService.update(TEST_CONTACT_ID, updatedContact);

		assertEquals(updatedContact.getName(), contact.getName());
		assertEquals(updatedContact.getEmail(), contact.getEmail());
		assertEquals(updatedContact.getAddress1(), contact.getAddress1());
		assertEquals(updatedContact.getAddress2(), contact.getAddress2());
		assertEquals(updatedContact.getCity(), contact.getCity());
		assertEquals(updatedContact.getState(), contact.getState());
		assertEquals(updatedContact.getZip(), contact.getZip());
		assertEquals(updatedContact.getCountry(), contact.getCountry());
		assertEquals(updatedContact.getAccount(), contact.getAccount());
	}
}
