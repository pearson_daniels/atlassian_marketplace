package com.pearson.atlassian.controller;

import com.pearson.atlassian.model.Contact;
import com.pearson.atlassian.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/contacts")
public class ContactController {
    @Autowired
    private ContactService contactService;

    @GetMapping("/{id}")
    public ResponseEntity<Contact> findById(@PathVariable Long id) {
        return contactService.getContactById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Contact> create(@RequestBody Contact contact) {
        return contactService.add(contact);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Contact> update(@PathVariable("id") long id, @RequestBody Contact contact) {
        return new ResponseEntity<>(contactService.update(id, contact), HttpStatus.OK);
    }
}
