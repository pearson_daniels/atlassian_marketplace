package com.pearson.atlassian.controller;

import com.pearson.atlassian.model.Account;
import com.pearson.atlassian.model.Contact;
import com.pearson.atlassian.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/accounts")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @GetMapping("/{id}")
    public ResponseEntity<Account> getAccount(@PathVariable Long id) {
        return accountService.getAccountById(id);
    }

    @GetMapping("/{id}/contacts")
    public ResponseEntity<Set<Contact>> getContactsForAccount(@PathVariable Long id) {
        return accountService.getContactsForAccount(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Account> createAccount(@RequestBody Account account) {
        return accountService.add(account);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Account> updateAccount(@PathVariable("id") long id, @RequestBody Account account) {
        return new ResponseEntity<>(accountService.update(id, account), HttpStatus.OK);
    }
}
