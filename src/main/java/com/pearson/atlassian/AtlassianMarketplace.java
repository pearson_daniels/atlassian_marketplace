package com.pearson.atlassian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;


@EntityScan("com.pearson.atlassian.model")
@SpringBootApplication
public class AtlassianMarketplace {

	public static void main(String[] args) {
		SpringApplication.run(AtlassianMarketplace.class, args);
	}

}


