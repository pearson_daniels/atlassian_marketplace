package com.pearson.atlassian.service;

import com.pearson.atlassian.exception.ResourceNotFoundException;
import com.pearson.atlassian.model.Account;
import com.pearson.atlassian.model.Contact;
import com.pearson.atlassian.persistence.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;

@Component
public class AccountService {
    @Autowired
    AccountRepository accountRepository;

    public ResponseEntity<Account> add(Account account) {
        return new ResponseEntity<>(accountRepository.save(account), HttpStatus.OK);
    }

    public Account update(long id, Account account) {
        return accountRepository.findById(id)
                .map(_account -> {
                    _account.setCompanyName(account.getCompanyName());
                    _account.setAddress1(account.getAddress1());
                    _account.setAddress2(account.getAddress2());
                    _account.setCity(account.getCity());
                    _account.setState(account.getState());
                    _account.setZip(account.getZip());
                    _account.setCountry(account.getCountry());
                    return accountRepository.save(_account);
                })
                .orElseGet(() -> accountRepository.save(account));
    }

    public ResponseEntity<Account> getAccountById(long id) {
        Optional<Account> account = accountRepository.findById(id);
        if (account.isPresent()) {
            return new ResponseEntity<>(account.get(), HttpStatus.OK);
        }
        throw new ResourceNotFoundException("Couldn't find an Account with id: " + id);
    }

    public ResponseEntity<Set<Contact>> getContactsForAccount(long id) {
        Optional<Account> account = accountRepository.findById(id);
        if (account.isPresent()) {
            return new ResponseEntity<>(account.get().getContacts(), HttpStatus.OK);
        }
        throw new ResourceNotFoundException("Couldn't find an Account with id: " + id);
    }

}