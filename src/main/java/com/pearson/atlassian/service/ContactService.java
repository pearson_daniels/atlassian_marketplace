package com.pearson.atlassian.service;

import com.pearson.atlassian.exception.ResourceNotFoundException;
import com.pearson.atlassian.model.Contact;
import com.pearson.atlassian.persistence.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ContactService {
    @Autowired
    ContactRepository contactRepository;

    public ResponseEntity<Contact> add(Contact contact) {
        return new ResponseEntity<>(contactRepository.save(contact), HttpStatus.OK);
    }

    public ResponseEntity<Contact> getContactById(long id) {
        Optional<Contact> contact = contactRepository.findById(id);
        if (contact.isPresent()) {
            return new ResponseEntity<>(contact.get(), HttpStatus.OK);
        }
        throw new ResourceNotFoundException("Couldn't find an Contact with id: " + id);
    }

    public Contact update(long id, Contact contact) {
        return contactRepository.findById(id)
                .map(_contact -> {
                    _contact.setName(contact.getName());
                    _contact.setEmail(contact.getEmail());
                    _contact.setAddress1(contact.getAddress1());
                    _contact.setAddress2(contact.getAddress2());
                    _contact.setCity(contact.getCity());
                    _contact.setState(contact.getState());
                    _contact.setZip(contact.getZip());
                    _contact.setCountry(contact.getCountry());
                    _contact.setAccount(contact.getAccount());
                    return contactRepository.save(_contact);
                })
                .orElseGet(() -> contactRepository.save(contact));
    }
}